# MAC121-EP1
> An implementation of Caesar's cipher in C for the subject MAC121 - Algorithms  and Data Structures I (2020).

## How to run it locally?
In order to compile and run this project, you can simply execute the following snippet in the project's root directory:
```
make run
```

Notice that we are using the GCC with the following arguments:
```shell script
gcc -Wall -ansi -pedantic -O2 -o ep1 src/ep1.c
```

## How to check memory leaks and segmentation faults?
We are using Valgrind to make sure there is no memory leaks and enhance segmentation fault detection and solving. You can follow [these guides](https://stackoverflow.com/questions/24935217/how-to-install-valgrind-good/51671524) to install it in your computer. To scan this program with Valgrind, just run this against the project's source.
```
make check
```

## Authors
* 11796378 - [João Henri](joao.henri@usp.br)