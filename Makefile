_clear:
	clear

_build: _clear
	gcc -Wall -ansi -pedantic -O2 -o ep1 src/ep1.c

check: _build
	valgrind --leak-check=full ./ep1

run: _build
	./ep1
