#include <stdio.h>
#include <stdlib.h>
#include "ep1.h"

char* readArchiveName() {
    char* archiveName = malloc(80 * sizeof(char));

    printf("Type the name of the archive to be encrypted: ");
    if (scanf ("%s", archiveName) == 0) {
        printf("Failed to read archive name.");
        exit(EXIT_FAILURE);
    }
    return archiveName;
}

char* readMessage(char* archiveName) {
    FILE* archive;
    char* message;
    long length;

    archive = fopen(archiveName, "r");
    if (archive == NULL) {
        printf("Failed to find archive with name: %s\n", archiveName);
        exit(EXIT_FAILURE);
    }

    fseek(archive, 0, SEEK_END);
    length = ftell(archive);
    fseek(archive, 0, SEEK_SET);

    message = malloc(length + 1);
    message[length] = '\0';

    if (!(fread(message, 1, length, archive))) {
        printf("Failed to read file.");
        exit(EXIT_FAILURE);
    }
    fclose(archive);
    return message;
}

caesarCipherKey readCaesarCipherKeys() {
    caesarCipherKey caesarCipherKey;

    printf("Which will be the offset for lowercase characters?\n");
    if (scanf ("%d", &caesarCipherKey.lowercaseOffset) == 0) {
        printf("Failed to read lowercase offset.");
        exit(EXIT_FAILURE);
    }

    printf("Which will be the offset for uppercase characters?\n");
    if (scanf ("%d", &caesarCipherKey.uppercaseOffset) == 0) {
        printf("Failed to read uppercase offset.");
        exit(EXIT_FAILURE);
    }

    if (caesarCipherKey.lowercaseOffset < 0 || caesarCipherKey.uppercaseOffset < 0) {
        printf("Uppercase and lowercase offset must be greater or equal 0.\n");
        exit(EXIT_FAILURE);
    }

    return caesarCipherKey;
}
char* mapSpecialCharacterCode(char specialCharacter) {
    switch (specialCharacter) {
        case ',':
            return "vr";
        case '.':
            return "pt";
        case ':':
            return "dp";
        case '!':
            return "ex";
        case '?':
            return "in";
        default:
            return "nl";
    }
}

int isUppercase(char character) {
    return character >= 'A' && character <= 'Z';
}

int isLowercase(char character) {
    return character >= 'a' && character <= 'z';
}

int isSpecialCharacter(char character) {
    return character == ',' || character == '.' || character == ':' ||
        character == '!' || character == '?' || character == '\n';
}

int getEncryptedMessageSize(stringMetadata stringMetadata) {
    return stringMetadata.originalMessageSize - stringMetadata.totalSpaces + stringMetadata.totalSpecial;
}

stringMetadata getStringMetadata(char* message) {
    struct stringMetadata stringMetadata;
    int counter;

    stringMetadata.totalSpaces = 0;
    stringMetadata.totalSpecial = 0;
    stringMetadata.originalMessageSize = 0;

    for (counter = 0; message[counter]; counter++) {
        if (isSpecialCharacter(message[counter])) {
            stringMetadata.totalSpecial++;
        }

        if (message[counter] == ' ') {
            stringMetadata.totalSpaces++;
        }

        stringMetadata.originalMessageSize++;
    }

    return stringMetadata;
}

char getEncryptedCorrespondent(char originalCharacter, int lowercaseOffset, int uppercaseOffset) {
    char encryptedCharacter;

    if (isLowercase(originalCharacter)) {
        encryptedCharacter = originalCharacter + lowercaseOffset;

        while (!isLowercase(encryptedCharacter)) {
            encryptedCharacter = encryptedCharacter - 26;
        }
        return encryptedCharacter;
    }

    encryptedCharacter = originalCharacter + uppercaseOffset;
    while (!isUppercase(encryptedCharacter)) {
        encryptedCharacter = encryptedCharacter - 26;
    }
    return encryptedCharacter;
}

char* encryptMessage(stringMetadata stringMetadata, caesarCipherKey caesarCipherKey, const char* message) {
    int messageIterator;
    int encryptedMessageIterator;
    int encryptedMessageSize;
    char* encryptedMessage;
    char* specialCharacterCode;

    encryptedMessageSize = getEncryptedMessageSize(stringMetadata);

    encryptedMessage = malloc((encryptedMessageSize + 1) * sizeof(char));
    encryptedMessage[encryptedMessageSize] = '\0';

    encryptedMessageIterator = 0;
    for (messageIterator = 0; message[messageIterator]; messageIterator++) {
        if (isSpecialCharacter(message[messageIterator])) {
            specialCharacterCode = mapSpecialCharacterCode(message[messageIterator]);

            encryptedMessage[encryptedMessageIterator] =
                    getEncryptedCorrespondent(specialCharacterCode[0],
                                              caesarCipherKey.lowercaseOffset,
                                              caesarCipherKey.uppercaseOffset);
            encryptedMessageIterator++;
            encryptedMessage[encryptedMessageIterator] =
                    getEncryptedCorrespondent(specialCharacterCode[1],
                                              caesarCipherKey.lowercaseOffset,
                                              caesarCipherKey.uppercaseOffset);
            encryptedMessageIterator++;
        }
        else if (message[messageIterator] != ' ') {
            encryptedMessage[encryptedMessageIterator] = getEncryptedCorrespondent(
                    message[messageIterator],
                    caesarCipherKey.lowercaseOffset,
                    caesarCipherKey.uppercaseOffset);
            encryptedMessageIterator++;
        }
    }
    return encryptedMessage;
}

int main() {
    char* message;
    char* archiveName;
    char* encryptedMessage;
    stringMetadata stringMetadata;
    caesarCipherKey caesarCipherKey;

    archiveName = readArchiveName();

    message = readMessage(archiveName);
    caesarCipherKey = readCaesarCipherKeys();

    stringMetadata = getStringMetadata(message);
    encryptedMessage = encryptMessage(stringMetadata, caesarCipherKey, message);

    printf("Encrypted message is: \n%s\n", encryptedMessage);

    free(archiveName);
    free(message);
    free(encryptedMessage);
    return EXIT_SUCCESS;
}
