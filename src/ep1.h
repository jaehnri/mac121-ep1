#include <stdio.h>

typedef struct stringMetadata {
    int totalSpaces;
    int totalSpecial;
    int originalMessageSize;
} stringMetadata;

typedef struct caesarCipherKey {
    int lowercaseOffset;
    int uppercaseOffset;
} caesarCipherKey;

int isSpecialCharacter(char character);
int isUppercase(char character);
int isLowercase(char character);
int isSpecialCharacter(char character);
char* mapSpecialCharacterCode(char specialCharacter);

caesarCipherKey readCaesarCipherKeys();
char* readMessage(char archiveName[]);
char* readArchiveName();

stringMetadata getStringMetadata(char* message);
int getEncryptedMessageSize(stringMetadata stringMetadata);
char getEncryptedCorrespondent(char originalCharacter, int lowercaseOffset, int uppercaseOffset);
char* encryptMessage(stringMetadata stringMetadata, caesarCipherKey caesarCipherKey, const char* message);